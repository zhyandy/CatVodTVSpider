## 共享包

### 这里的jar包由群友玩家共享的爬虫代码生成，不定期更新jar包和配置

### **注意：由于防火墙的问题，你可能无法直接使用这里的jar包，请自己想办法解决**

```json

"spider": "https://github.com/catvod/CatVodTVSpider/blob/master/jar/custom_spider.jar?raw=true",

{
  "key": "csp_Aidi",
  "name": "爱迪",
  "type": 3,
  "api": "csp_Aidi",
  "searchable": 1,
  "quickSearch": 0,
  "filterable": 1
},
{
  "key": "csp_Enlienli",
  "name": "嗯哩嗯哩",
  "type": 3,
  "api": "csp_Enlienli",
  "searchable": 1,
  "quickSearch": 0,
  "filterable": 1
},
{
  "key": "csp_EPang",
  "name": "阿房影视",
  "type": 3,
  "api": "csp_EPang",
  "searchable": 1,
  "quickSearch": 0,
  "filterable": 1
},
{
  "key": "csp_Auete",
  "name": "Auete",
  "type": 3,
  "api": "csp_Auete",
  "searchable": 1,
  "quickSearch": 0,
  "filterable": 1
}
```
